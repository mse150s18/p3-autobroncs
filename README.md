Instructions:
---------------------------------------------------------------------------------------------------------------------

Our team decided to tackle the project of calculating the grain size distribution of an image. Our code may be run by asking python to run the grain_counter.py file. This script analyzes the provided image, plots a histogram of grain size and determines the average grain size of the image. Lastly, the code will also provide a comparison of the histogram created with a Gaussian distribution function. A more detailed explanation of the workings of the code is provided below:

Please make sure to install the 'OpenCV' library onto your machine before running the code.

To do this on the school machines enter the following line:
$ pip install --user opencv-python

The code may also output the following error from the OpenCV library upon running the program but can be ignored:
libpng warning: iCCP: known incorrect sRGB profile

The general idea behind our team's code involves first changing the color of the image so that it is strictly in black and white. Then the code will pick a random point on the image and test if the pixel there is black or white. If the pixel is white, it will pick a different point, until it finds a black pixel. The black pixels are the grain edges which is where the code needs to start in order to measure the distance across a grain. The purpose of this process is to ensure we do not start counting in the middle of a grain. 

Once finding a black pixel, the code will move in a random direction and test each pixel it encouters to see if that pixel is black or white. As it moves across the image, it will count how many white pixels it encounters and record that number until it hits another black pixel, or another grain boundary. Then the process starts over again, compiling a data set of grain lenghts, or diameters. 

This code will run a large number of times, picking random points and scanning in a direction (up, down, right or left) and compiling an array of grain dimensions. Next, we trim the data and remove outliers by requiring all data to be larger than 20 pixels and less than 4 standard deviatons from the sample median. Assumming that the average grain size is circular, we may use this trimmed data to calculate the area of all the grains and then average this value to determine average grain size. From the array of grain size we may create a histogram to show the distribution. Lastly, we graphed a Gaussian distribution function with our histogram to compare how closely our data matched this theoretical distribution. 


