#create a loop to move around the image, choose an image and determine if the pixel is black or white. If the pixel is white, count across until reaching a black pixel. If the pixel is black, redo to choose another random pixel. Repeat to obtain a random sampling of diameter. Average random distance and (assuming most grains are square/circular) we can use the average dimension to calculate average grain size. A histogram may be created from the random sampling of grain width. 

# ---------- Start - Pulled from Jankowski File ----------------
# -- for loops

#from scipy import misc ---imports the miscilanious routines (commands with no home) from the scipy library which short for science python. The library for linux mathmatics for science and engineering---
#import matplotlib.pyplot as plt ---- creates a short lingo 'plt' to refer to matplotlib.pyplot for importing with out having to type out a bunch of code.
#from numpy import random    ---- imports numpy.random library (a library with useful codes for array manipulation and basic functions for mathmatics.)
#a = misc.imread('grains.png',flatten=True)--- sets the misc.imread(file) command to the refferance 'a'. misc.imread pulls the image data from the "grains.png' file and turns the pixel values from an RGB to greyscale with "flatten=true".
#print(a.max(),a.min())---- asking to print only the maximum and minimum pixel values of the image file. 
#a[a>128]=255 ---- runs through the misc.imread command and rewrites the pixel values. For grey scale value greater than 128, the value is rewritten as the maximum value "white". 
#a[a<=128]=0 --- rewrites the pixel values from 'grains.png' using 'a'. For values less than 128 they are rewritten as the minimum value 'black'. 
#plt.imshow(a) ---- plt.imshow(a) draws the new image using the 'a' that we have defined with maximum and minimum values.
#plt.show() ---- plt.show() prints the image. With out this command we would not see out picture.

# -- generate random points
#def randpoint(array):
#	x = randint(array.shape[1])
#	y = randint(array.shape[0])
#	return array[y,x]

#stepping examples
# -- this code doesn't include error-checking for the edges of our array
# -- the row and column can be used in  'image[row,column]'
#row,column = 3,14
# -- to step one 'east'
#row,column = row,column+1
# -- to step one 'west
#row,column = row,column-1
# -- to step one 'north'
#row,column = row+1,column
# -- to step one 'south'
#row,column = row-1,column

# # ---------- End - Pulled from Jankowski File ---------------

#----------------Start- Command to create histogram for data set------------
#import matplotlib.pyplot as plt
#import numpy as np
#data_set=
#plt.hist(data_set, bins = 100) 
#plt.title("Grain Size Histogram")
#plt.xlabel("grain size (um)")
#plt.ylabel("# of data")
#plt.show()
#----------------End-  Command to create histogram for data set-------------

#-----------------------Start -- Fitting Distributions -----------------------
#After we have a histogram plot we can plot a distribution function over it to show how well our data matches. 
#-----------------------End -- Fitting Distributions -----------------------------

#------------------------Start -- Grain Size Calculations ---------------------------

import numpy

elements = numpy.array(data_set)

mean = numpy.mean(elements, axis=0)
std = numpy.std(elements, axis=0)

new_data = [x for x in data_set if (x > mean -2 *std)]
new_data = [x for x in new_data if (x < mean +2 *std)]

new_data = np.array(new_data)
print(new_data)

new_data_um = new_data * (200/290)

grain_area = (new_data_um/2)^2*np.pi

print(np.average(grain_area),'um^2')

