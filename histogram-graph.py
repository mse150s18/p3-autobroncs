import matplotlib.pyplot as plt
import numpy as np

data_set=  #sets data with folder/filename

plt.hist(data_set, bins = 2) #makes data_set in to 100 parts, will have 100 bars in the graph

plt.title("Grain Size Histogram") #titles plot with Grain Size Histogram

plt.xlabel("grain size (um)") #labels x named grain size (um)

plt.ylabel("# of data") #labels y name with # of data

plt.show() #this will show the plot that is created above

