#images are made of pixels. Pixels are made of channels. Channels have dimensions which have values for (red, green, blue, alpha). AAlpha is a tansparency value. Greyscale images have 1 dimension. 
#plan: load png. Use values to determine edges. run program to measure distances between edges. Plot distance measrurements and solve for average.
#image size 1540x2072 pixels
#200um=290pixels
#1pixel=0.689655172um
import matplotlib.pyplot as plt
import scipy.ndimage
import numpy as np
image=scipy.ndimage.imread('grain-data/grains.png',flatten=True)
gw= 0 #sets grain width 'gw' initial value to 0. in pixel values. need ot convert to length.  
p=0.68955 #defines one pixel length in micrometers and sets it as the constant p.
grain_width_list=[] #this code creates a list called grain_width_list that [] defines as empty.
for j in range(1540): #this defines number of points in the j direction
	for i in range (2072): #this defines the number of points in the i direction.
		if (image[j,i] >=80): #this sets an parameter if greater than or equal to 95
			gw+=p #when previous argument is true adds value p to value gw
		if (image[j,i] <80):#if pixel is less than 95 
			if (gw!=0):#if gw in not =0 then append 
				grain_width_list.append(gw) #append list 
				gw=0 #reset gw value to 0
grain_width_list=np.array(grain_width_list) #creates an array with the values from the grain width list
grain_area_list=(grain_width_list/2)**2*np.pi #inputs the grain width list data into a circle area function
plt.hist(grain_area_list,bins=30,range=[0,1500], facecolor='red', align='mid')
d=np.median(grain_width_list)#defines mean_grain_width with numpy mean function value. 
r= d/2 #defines the radius 
a=(r)**2*np.pi #a= the area of a circle with the radius of the median grain diameter divided by 2 
print(a)#prints the grain area of the median grain size
print(d)#lists the mean value for the grain width when script is ran
plt.show()#tells python to show the histogram of the data requested.

# the concept of this approach is is to convert the image pixel data into a greyscale. I printed the first line of pixel value to understand the range of intensities. White = 255. and grain boundary ~= 75-95. Using this threshold to identify a 'for' loop command to analyze the image. Calculating the pixel scale to 1 pixel = 0.68955172 micrometers; this value was added to the grain width for each pixel in the grain. This code counted consecutive pixels above the threshold. When the scan ran into a grain boundary it appended the gran width list with the value it had stored then reset it to zero. and continuted forward until the next grain pixel. Statisically speaking you would get multiple virtual grains compared to actual grains. So this method would not satisfy finding the grain count, but focuses on the average size.It then prints the median grain width. It also prints the area of a circle with the radius of the median grain width. 
#that is all. As mince buckner once said. Get out of my life cow! 
