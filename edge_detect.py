#Script for calculating various grain information from an image. The script will countour the image and turn it into a black and white image. Then the script will walk across various grains randomly and determine the length of the grain. Further grain analysis is done from the length of the grains collected.

#OpenCV needs to be installed for this program to run. To do this on our machines enter the following line:
#$ pip install --user opencv-python

#When running this script you may encounter the error listed below which can be ignored:
# libpng warning: iCCP: known incorrect sRGB profile

import cv2
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.mlab as mlab
from scipy.stats import norm

im = cv2.imread('grain-data/grains.png',0)	#This reads the desired images using OpenCV
#print(im.shape)				#Prints the shape of the image (Pixels x Pixels)
#imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)	#Converts the image to grayscale, is grayscale already
ret,thresh = cv2.threshold(im,127,255,0)	# Sets threshold for contouring
im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)#Defines our new variable for the image (im2), contours, and hierarchy. Uses the OpenCV library to run the libraries to countour the image.
cv2.drawContours(im2,contours,-1,(0,255,0),2) #Draws the new contour image for im2 with the values. To change the intensity of the contour, change the last value in the line from somehwere arond 1 - 5. Currently it is set to 2.
#print(im2[1000,100])	#this was use to test and see if it could determine what value was at the specified pixel. Commented out for now as it was just used for testing.
#cv2.imwrite("contours_test.png",im2)	#This will write the contoured image to a file. Useful for testing the intensity of the contour above to find a happy medium. Commented out for now since it was used for testing.

def randpoint(array):	#Argument for choosing a random point on the image
	x = np.random.randint(100,1440)	#Selects an x point from 100 to 1440 pixels, chose slightly less than entire image so it has some room to walk
	y = np.random.randint(100,1972)	# selects a y point from 100 to 1972 pixels, chose slightly less than entire image so it has some room to walk
	return array[x,y],x,y #Returns an xy array as well as individual x and y points

def randdirect():	#Argument for selecting a random point to choose a direction in the step function argument
	direction = np.random.randint(0,4)	#Selects a random number from 1 to 3
	return direction			#Returns that value
	

def step(x,y,direction):	#Argument for selecting a direction to walk. Inputs are an x and y value, also the direction (number) from the randdirect argument
	if direction == 0: #Up
		x = x+1
	if direction == 1: #Down
		x = x-1
	if direction == 2: #Left
		y = y-1
	if direction == 3: #Right
		y = y+1
	return x,y		#Returns the direction for the walk
	

def grainsize(n):
	i = 0	#This value is used for counting the number of loops, defining as 0 to initate it
	findpixel = 0	#Variable that will be used to look for black pixels (0). White is 255.
	sizes = []
	while i < n:	#While statement that runs if i is less than 50, defined as 0 at the start
		numberwhite = 0	
		findpixel = 0	#Defines findpixel as 0, meaning it is looking for a 0 (black) value
		pixel,x,y = randpoint(im2) #Runs the randpoint argument above. Defines pixel as the xy array.
		while findpixel != 1: #While statement that runs as long as findpixel does not equal 1
			if pixel == 255: #If statement that runs if pixel color is 255, which is white, we want black
				pixel,x,y = randpoint(im2)#Runs the randompoint argument again to get a new random pixel
			else:	#else statement that looks for a pixel of 0, which is black and what we want
				direction = randdirect() #Defines direction as one chosen from the randdirect argument
				while pixel == 0 and y < 2072 and y > 0 and x < 1540 and x > 0: #Makes sure pixel is black and within the bounds of the image
					x,y = step(x,y,direction)	#Selects a direction to step
					pixel = im2[x,y]		#Checks the pixel color at that point
				while pixel == 255 and y < 2072 and y > 0 and x < 1540 and x > 0: #Makes sure pixel is white and within the bounds of the image
					numberwhite += 1	#Counter used for white pixels, increases by 1 each step
					x,y = step(x,y,direction)	#Selects a direction to count
					pixel = im2[x,y]		#Checkes the pixel color at that location
				findpixel = 1 #Change findpixel to 1 since we found a black pixel and it can stop running this loop
				sizes.append(numberwhite)	#Appends the number of white pixels counted to the array
		i += 1	#Increments i by one each time it loops through the process. Until a defined number of loops (n) specified at s=grainsizes(n)

	return(sizes) #Returns the diamters measured in the script (appends the values to a list)

s = grainsize(2000)	#Defines s as an array and tells it to run the loop a certain number of times and append the values.

print("Grain width without STD: ",np.median(s)," pixels") #Prints the average diameter without any standard deviation correction in pixels
#elements = numpy.array(data_set)

mean = np.mean(s, axis=0)
std = np.std(s, axis=0)

new_data = [x for x in s if (x > mean -1 * std)]
new_data = [x for x in new_data if (x < mean +3 * std)]

new_data = np.array(new_data)
print(new_data)

new_data_um = new_data * (200/290)
print(new_data_um)

grain_area = (new_data_um/2)**2*np.pi

print(np.median(grain_area),'um^2')

grains = ((1540*0.689655)*(2072*0.689655)*0.7) / np.median(grain_area)
print(grains," grains in the image")

plt.hist(grain_area, bins = 20) #makes data_set in to 20 parts, will have 100 bars in the graph

(mu,sigma) = norm.fit(grain_area)

n,bins,patches=plt.hist(grain_area,20,normed=1,facecolor='green',align='mid')

y= mlab.normpdf(bins,mu,sigma)
plt.plot(bins,y,'r-',linewidth=2)

plt.title("Grain Size Histogram") #titles plot with Grain Size Histogram

plt.xlabel("grain size (um^2)") #labels x named grain size (um)

plt.ylabel("# of data") #labels y name with # of data

plt.show() #this will show the plot that is created above
