 

#Ariel:

1.	threshold = 100

	This command defines the variable 'threshold' with the value 100. 100 is a pixel value corresponding to color, in this case a shade of grey. 


2.	if image[j*50] <= threshold and image[j*50, i+1] > threshold:

	This line of code is a process for counting grains depending on their color. This line will be satisfied if both the conditions to the left and right of the inequality signs are true. The first section tells python to move to the position j50,i (specifying a row and column coordinate) within 'image', then determine if that section is less than and equal to the specified threshold. The far right side of the image asks python to move to the position j50, i+1 and determine if that section is larger than the threshold. If both of these conditions are true, then that section is a grain.  


3.	grains+=1

	Earlier in the code we specificed that the initial count of grains is zero. This code is a tool for counting the grains determined using the code directly above. If a pixel is a grain it will add one to the running tally of grains and store it in the variable 'grains.' 


4.	 mean = np.mean(s, axis=0)

	This line of code determines the mean of the array 's' and stores it in the variable 'mean.' Setting the axiss = 0 specifies the dimension index (rows). 


5.	new_data = [x for x in s if (x > mean-1*std)]


	This line of code defines a new variable, new_data, that will be used for storing the values extracted from 's' using a conditional statement. This code takes in all the values of 's' and runs them through the condition x> mean-1*std, which determines if the value in question is larger than the the mean value minus one multiplied by the standard deviation. If this is true, this value will be stored in new_data. 


6.	new_data = [x for x in new_data if (x < mean+3 *std)]

	This code further narrows down the values within new_data by setting the condition that all values must also be less than the mean +3 multiplied by the standard deviation. If this is still true, the value will stay within new_data but will otherwise be removed. 


7.	new_data = np.array(new_data)

	This line uses the command np.array on the variable new_data to turn new_data into an array. 	


8.	new_data_um = new_data *(200/290)

	This line of code transforms our data set from pixels to um using the conversion factor 200/290. Because new_data is now an array, we can multiply it by the conversion factor and every value within new_data will be multiplied and stored in a new array, new_data_um. 


9.	grain_area = (new_data_um/2_**2*np.pi

	This command is used to find the average area of grains using the values calculated in new_data_um. The new_data_um arrray is divided by two and squared, then multiplied by the numpy value np.pi. This calculation will be performed on all values within the new_data_um array and stored in a new array, grain_area. 


10.	print(np.mean(grain_area),'um^2') 

	This line first tells python to determine the mean value from the array grain_area. After finding this value, python will perform the 'print' function to show that value on the screen. The last part of the coommand ",um^2'" will allow um^2 to be printed after the calculated mean value. 


----
#Claire:

1.	import matplotlib.pyplot as plt 

	Python knows what matplotlib is because this command is in it's massive library. However for our specific code we need to bring this library in so that we can do specific things to our data that we couldn't do otherwise. By importing the matplotlib module, a tool is brought in for the user which allows them to graph data, which is similar to a MATLAB type plot setting. This command can be imported as plt, which is just a way of abreviating the command so it is easier to type in later commands.


2.	def randdirect():
	direction = np.random.randint(0,4)	
	return direction

	The first line defines the argument which we have named randdirect where a random direction will eventually be chosen. This argument defines a specific function that we can use later.
	The np.random.randint commmand will store information about the specific direction chosen. The numbers inside the parenthesis denote the specific directions that can be chosen, which are labeled in another line of code, but basically the numbers 0-3 will make the code go up, down, left, right. Those numbered directions are run through the np.random.randint function and stored.
	The last line just spits out the direction value which will be a number from 0 to 3. 


3.	def randpoint(array):
        x = np.random.randint(100,1540)
        y = np.random.randint(100,2072)
        return array[x,y],x,y
	
	The first line of this command explains the what the function is and how we are simply defining or pinpointing a random point on the grain image.
	The next two lines are defining the range of pixels on the grain image that the random point can be picked from in the x and y directions.
	The third line will give the output from the random point on the grain image. The value inside the brackets will detect the grayscale level of the point, in other words is the point black or white. Then the x and y values after the square brackets explain the actual position of the point in the x and y direction on the grain image. 


4.	print(im.shape)

	In this case the stuff inside the parenthesis, the image shape/size  of (pixels X pixels), is run through the print function. This command will simply take our grain image from Bitbucket and print it out so that we can see it. The image will "print" out and pop up in a separate little window.


5.	imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY) 

	The stuff on the left of the equals sign is simply defining a name to our variable stating that our image will be in grayscale. The stuff on the right of the equals sign outside the parenthesis is just our function. It states that the color of our image will be converted to another color. Then the stuff inside the parenthesis is giving parameters for the function. The grain image to be converted to stricktly black and white.  


6.	ret,thresh = cv2.threshold(im,127,255,0)

	The left side of the equals sign defines the variable in which there will be two outputs, the return value  and the threshold image. The cv2.threshhold states that there is an image we are looking at and we will denote the pixel values for our threshold. The stuff inside the parenthesis is stating the range on pixels on the image that we are focused on. 


7.	im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

	Here there are three variables that are being defined using the stuff on the right side of the equals sign. When this command is run, there will be three outputs respectfully. The cv2.findCountours function takes in the three things inside the parenthesis and runs it through. The data is run through the cv2.findContours function and the outputs are then stored in the three variables on the left of the equals sign. Because the same function is being used to run data and store in the three variables, we are able to create this one line of code instead of three lines for each variable.     


8.	import cv2

	CV stands for "computer vision", and in order to be able to run our code in the first place, we must import cv2 which will allow us to do all the cool stuff to our image later, including pinpointing a random spot and counting pixels to measure grain size. 


9.	std = np.std(s, axis=0)
	
	The std is just a variable being defined, stating that the standard deviation will be found using the stuff on the right of the equals sign. Then the np.std is the numpy standard deviation function which is basically like a calculator which will find the standard deviation of our data for us. Then the stuff inside the parenthesis is the series of data that will be run through the np.std function, where the s is the variable input for the length of each grain, and axis=0 specifies dimension index of rows in the grain image. 
	

10.	row,column = 3,14
	 -- to step one 'east'

	This command denotes a row and column on the grain image, in this case 3 and 14 respectfully. The rows and columbs correspond to the pixels in the grain image, so when the row and a column have been defined usng the equals sign, the next step is picking a direction that we want our code to travel in order to check for grain boundaries. The two lines -- means that a for loop is being used, which will repeat running our code a number of times. The line where it says "to step one 'east'" is allowing the code to continue looking at the same row of pixels, but moves over one column in the east direction. 

----
#Jiheon:

1.	from scipy import misc

	Python uses 'import' to import and find misc file from the scipy program somewhere on my system. misc stands for miscellaneous. This command allows us to read and display our image of 'grains.png'.


2.	image = misc.imread('grain-data/grains.png', flatten=True)

	Python passes the variable 'image' as an argument to the 'imshow' function, which is in 'plt' object. This command allows us to set the image in grain-data folder named grains.png to be named 'image' for later use. 'flatten=True' will make the image to be flattened when we type 'image' into the script whenever we want to use image. 'flatten=True' also makes image flatten so that each pixel has a single data value and count pixels.


3.	plt.imshow(image)

	This intended to show an image from upper command using an argument to the 'imshow', which is in 'plt' object. From what Claire wrote in 1, command 'plt' imports 'matplotlib.pyplot' with the shortened word. command 'imshow(image)' lets us to show image grains.png from grain-data folder. This will show us the  grain image.


4.	data_set = folder/filename
	
	This passes the variable 'data_set' as an arguement to the folder/filename. This command lets us to put the file into the script named data_set, this will allow us to not write whole folder and filename whenever we want to use it to put data set to make a plot or use it for later use. This will make the script more shorter than writing all of the folder/filename in the script. data_set will contain all the grain sizes in the picture in data 'grain-set/grains.png'. It will contain all grain size used from other python script.


5.	plt.hist(data_set, bins = 20)

	This 'hist' function is in 'plt' object, this will pull up the data from 'data_set' and 'bins = 20' will separate the data in to 20 data that fits in to the graph. This command will plot histogram with matplotlib.pyplot system. This command will have many data in the data_set file including x as grain-size and y as number of the specific grain-size. Graph will show as a bar graph. Then there will be a histogram. 'bins = 100' bins command will control the number of bars in the histogram.


6.	plt.title(plot_title)
	
	This 'title' function is in 'plt' object, making a title for a graph that we are creating. Command plt will import matplotlib.pyplot and it will make the title shown above the graph. This will make name of the histogram plot as 'plot_title'. plt will import matplotlib.pyplot. The title will be above the histogram plot and it will be shown as plot_title for this particular plot that was created by 'plt.hist(data_set)'


7.	plt.xlabel(name)
	
	'xlabel' function is in 'plt' object, making a x-axis label for a graph that we are creating. Command plt will import matplotlib.pyplot and it will label the x-axis on the graph. This command will label x axis as 'name', such as in our data_set, it will be labeled as 'Grain Size (um)'. we will have various values for the x axis due to bins command above.


8.	plt.ylabel(name)

	'ylable' function is in 'plt' object, making a y-axis label for a graph that we are creating. Command plt will import matplotlib.pyplot and it will label the y-axis on the graph. This command will label y axis sd 'name', in our data, this will be labeled as "number of the grains" in our data.


9.	plt.show()

	'show' function is in 'plt' object and it will show the final version of commands that we wrote previously. It will show the graph. Command plt will import matplotlib.pyplot and it will show the data we plotted above. This will visually show the data plotted with data_set assigned above.


10. print(data.max(),data.min())
	
	'print' function will print the values that are in the (), which is 'data.max' and 'data.min'. 'data.max' will calculate the maximum value of the data, and 'data.min' will calculate the minimum value of the data that we are using. This command prints only the data's max and min when we use this command. The script will look like maxvalue first and then the min value. it will only show data's max and min and this command won't do anything else.


----
#Josh:

1. image=scipy.ndimage.imread('grain-data/grains.png', flatten=True)
	
	Sets the variable image to the function 'imread' from the library "scipy" in the sub library "ndimage"that reads the file data from 'grain-data/grains.png' and changes the RGB data, with flatten=True, to greyscale insentsity values.


2.	p=0.68955	

	defines p as the floating point value stated


3.	grain_with_list=[]

	This code defines grain_width_list as an array that [] defines as empty. This list is going to be populated with the gw values calculated in the 'for loop' defibned below.


4.	for j in range(1540)
		for i in range(2072)

	This 'for loop' defines the array parameters for the array values. 1540 in the j direction and 2072 in the i direction.  


5.	if (image[i,j] >= 95):

	This sets a true or false argument that if pixel [i,j] in image has value >= (greater than or equal to) 95 and sets up an action based on the output of the true or false with the : 


6.	gw+=p
	
	This adds the value p ,defined earlier, to the gw value, defined earlier, if the previous if argument is true. 
	

7.	if (image[i,j] <95):

	This sets a true or false argument that if pixel [i,j] in image has value < (less than) 95 and sets up an output based on the true or false result. 


8.	if gw!=0: 

	This sets a true or false argument, to be checked only if the previous  argument is found true, to determine if the gw value != (does not equal to) 0 and set up and output with the :


9.	grain_width_list.append(gw) 
	
	This code appends the grain_width_list with the value of gw if it is found true that the current pixel is les than 95 and gw is not =0. 


10.	gw=0

	This resets the value of gw back to 0 after the grain_width_list had been appended with the gw value.


----
#Nick:  


1.	cv2.Canny(img,50,100)  

	This passess the variable 'img', the number 50, and the number 100 as arguments to the Canny function that is inside the cv2 object.
	

2.	cv2.imwrite("processed_image.png",edges)

	This instructs the program to write the edge processed image to a file utilizing the cv2 object.


3.	if direction == 0: #Up
		x = x+1

	Checks the direction chosen. It then defines how x and y will be changed for the walk depending on the direction chosen in another argument. It outputs the walk direction.


4.	numberwhite += 1
	
	Instructs the program to increase the value of numberwhite by 1 each time it is run in the while loop.


5.	while findpixel != 1:

	While function that dictates it to run the code beneath it while the variable findpixel does not equal 1.


6.	x,y = step(x,y,direction)

	Instructs the program to retrieve the new x and y values after running the argument 'step' which utilizes the current x and y values along with a variable 'direction'.


7.	pixel = im2[x,y]
	
	Instructs the program to assign the pixel color value at the specified x and y point on the image to the value 'pixel'.


8.	while pixel == 0 and y < 2072 and y > 0 and x < 1540 and x > 0:

	While loop that instructs the program to run the code beneath it while it meets the following conditions. The pixel color must be black (0) and the x and y values must be within the bounds of the image.


9.	pixel,x,y = randpoint(im2)
	
	Instructs the program to define 3 variables: pixel, x, and y. These values are returned from the randpoint argument using the image as the input value.


10. 	sizes.append(numberwhite)

	Instructs the program to append the value given to 'numberwhite' to an array labeled as 'sizes'


