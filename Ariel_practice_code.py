#using code based off https://bitbucket.org/mse150s17/p3-team5/src
#To Do: (use python commands to calculate dimensions) change first loop to break the image up into equal amounts of squares
#calculate individual grain sizes using color percentages- have a box over the 200 um label on the image
#calculate average grain size 
#divide image area by the area of the dark box to calculate average amount of grain inside the covered area
#add average amount of grains in dark box with the rest of the image
#create plot of average grain size
#2072x1540
#>>> image.shape
#(1540, 2072)
#>>> print(image.shape[0])
#1540
#>>> print(image.shape[0]/20)
#77.0
#>>> print(image.shape[1]/20)
#103.6
#>>> print(image.shape[1]//20)
#103


import numpy as np
from scipy import misc
#Below imports the image "grain.png and flattens it so that each pixel has a single data value.
image = misc.imread('grain-data/grains.png', flatten=True)#imports image and flattens it

import matplotlib.pyplot as plt
plt.imshow(image)

#grain count
grains =0 #initial grain count

#color threshold #can be improved by breaking the image up into more organized pieces: Ex: divide into 20 boxes
threshold = 100 #pixel value corresponding to color, white=0 and black = 250 
for j in range(27): 
	for i in range(2070):
		if image[j*50,i] <= threshold and image[j*50,i+1] > threshold: #this counts the grains by determining if the pixel to the left of the one being addressed is of a darker or lighter color. 
			grains+=1
#200 um = 290 pixels
#sample area = (1540 pixels deep* 2072 pixels wide)*(200 um/ 290 pixels)
#sample area = 0.0000015177 square meters
area = 0.0000015177 #square meters 
print(str(grains) + ' per 1.5178e-6 sq m')
print(str(grains/(area)) + 'grains/m^2') #changing grains integer into a string to add it with sample area

reds = 0 #a way of double checking code to make sure it's counting grains the way it's supposed to. Grains counted should be changed to the color yellow.
totalpixels = 2070*1350

for i in range(1350):
	for j in range(2070):
		if image[i,j] > threshold:
			reds+=1
percentage = reds/(totalpixels)

grainsize = 1 / (percentage*grains/(area))
print('avg grainsize = ' + str(grainsize) + ' m^2')


#red  = 255
#blue = 000
#image[down, over]
#Yellow is 150+
#200 um = 290 pixels
plt.show()

#When you assign a certain color scheme to the grains image (Lets say black and white where grains are black), you can create a code which calculates the percentage of black in the image to the total area of the image. Then, create a code which finds the number of grains in the whole image. Then, dividing the number of grains by the percent of black in the image, this will give you the average size of the grains throughout the whole image. This is how we should* solve this problem. 
